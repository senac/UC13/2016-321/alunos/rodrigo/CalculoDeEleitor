/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.calculodeeleitor;

import br.com.rodrigo.calculodeeleitor.CalculadoraDeEleitor;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Diamond
 */
public class CalculadoraDeEleitorTest {
     @Test
    public void naoDeveSerEleitor() {
        int idade  = 15 ;
        CalculadoraDeEleitor calculadora = new CalculadoraDeEleitor();
        String resultado  = calculadora.calcular(idade);
        assertEquals(CalculadoraDeEleitor.NAO_ELEITOR, resultado);
          
    }
    
    @Test
    public void deveSerEleitor() {
        int idade  = 25 ;
        CalculadoraDeEleitor calculadora = new CalculadoraDeEleitor();
        String resultado  = calculadora.calcular(idade);
        assertEquals(CalculadoraDeEleitor.ELEITOR_OBRIGATORIO, resultado);
          
    }
    
    @Test
    public void deveSerEleitorFacultativoJovem() {
        int idade  = 16 ;
        CalculadoraDeEleitor calculadora = new CalculadoraDeEleitor();
        String resultado  = calculadora.calcular(idade);
        assertEquals(CalculadoraDeEleitor.ELEITOR_FACULTATIVO, resultado);
          
    }
    
    
    @Test
    public void deveSerEleitorFacultativoIdoso() {
        int idade  = 66 ;
        CalculadoraDeEleitor calculadora = new CalculadoraDeEleitor();
        String resultado  = calculadora.calcular(idade);
        assertEquals(CalculadoraDeEleitor.ELEITOR_FACULTATIVO, resultado);
          
    }
    
}
