/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.calculodeeleitor;

/**
 *
 * @author Diamond
 */
public class CalculadoraDeEleitor {
     public static final String NAO_ELEITOR = "Não Eleitor";
    public static final String ELEITOR_OBRIGATORIO = "Eleitor Obrigatório";
    public static final String ELEITOR_FACULTATIVO = "Eleitor Facultativo";

    public String calcular(int idade) {

        if (idade >= 18 && idade < 65) {
            return ELEITOR_OBRIGATORIO;
        } else if (idade < 16) {
            return NAO_ELEITOR;
        }

        return ELEITOR_FACULTATIVO;
        
        
//        if (idade < 16) {
//            return NAO_ELEITOR;
//        } else if (idade < 18 || idade >= 65) {
//            return ELEITOR_FACULTATIVO;
//        }
//        return ELEITOR_OBRIGATORIO;
//        
//
//    }
    
}
}
